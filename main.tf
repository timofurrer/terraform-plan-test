terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.8.0"
    }
  }

  backend "http" {}
}

provider "gitlab" {
  # Configuration options
}

variable "CI_PROJECT_ID" {
  type = string
}

data "gitlab_project" "main" {
  id = var.CI_PROJECT_ID
}

//resource "random_password" "main" {
//  length = 10
//}
//
//locals {
//  topics = join(", ", data.gitlab_project.main.topics)
//}
//
//output "path" {
//  value = "${data.gitlab_project.main.path_with_namespace}-${local.topics}-${nonsensitive(random_password.main.result)}"
//}